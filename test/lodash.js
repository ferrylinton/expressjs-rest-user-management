'use strict';

const lodash = require("lodash");

let name = "ferry";
let value;
let req = {};

console.log({
    name,
    "aaaaa": "bbbbbb"
});

console.log(value ? "111111" : "2222222");
console.log(lodash.isUndefined(value));
console.log(lodash.isUndefined(req.page));
console.log(lodash.isInteger(req.page));
console.log(lodash.isInteger("aaaaa"));
console.log(lodash.isInteger("1"));
console.log(lodash.isInteger(2));
console.log(lodash.isInteger(-3));
console.log(lodash.toInteger('   33 '));
console.log(lodash.toInteger('   -1 '));