const { User, Role, Authority, RevokedToken } = require('../models/index');
const { uid } = require('uid');
const bcrypt = require('bcryptjs');
const jsonwebtoken = require('jsonwebtoken');
const lodash = require("lodash");
const fs = require('fs');
const privateKey = fs.readFileSync('./keys/dev/private.key', 'utf8');

async function authenticate(req) {
    let { username, password } = req.body;

    let user = await User.findOne({
        include: {
            model: Role,
            as: 'role',
            include: {
                model: Authority,
                as: 'authorities',
                attributes: {
                    exclude: []
                },
                through: {
                    attributes: []
                }
            }
        },
        where: { username }
    });

    if (user) {
        if (bcrypt.compareSync(password, user.passwordHash)) {
            let exp = process.env.JWT_EXPIRES_IN;
            let token = jsonwebtoken.sign(
                {
                    id: user.id,
                    username: username,
                    permissions: user.role.authorities.map(authority => authority.name)
                },
                privateKey,
                {
                    algorithm: 'RS256',
                    expiresIn: lodash.isInteger(exp) ? parseInt(exp) : exp,
                    issuer: process.env.JWT_ISSUER,
                    jwtid: uid(32)
                }
            );

            return { token }
        }
    } else {
        return null;
    }

}

async function revoke(req) {
    let token = fromHeaderOrQueryString(req);

    if (token) {
        var decoded = jsonwebtoken.decode(token);

        return await RevokedToken.create({
            id: decoded.jti,
            token: token,
            username: req.user.username
        });
    } else {
        return { token };
    }
}

function fromHeaderOrQueryString(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
        return req.headers.authorization.split(' ')[1];
    else if (req.query && req.query.token)
        return req.query.token;

    return null;
}

module.exports = {
    authenticate,
    revoke
};