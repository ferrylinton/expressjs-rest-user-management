const pageUtil = require('../util/page-util');
const auditLogService = require("../services/audit-log-service");
const Sequelize = require('sequelize');
const NotFoundError = require('../error/not-found-error');
const { AUTHORITY } = require("../config/constant").ActionModel;
const { CREATE, UPDATE, DELETE } = require("../config/constant").ActionType;
const { Op } = require('sequelize');
const { Authority } = require('../models/index');


async function findAndCountAll(req) {
  let params = pageUtil.getParams(req);
  params.order = [
    ['name', 'ASC'],
  ]

  if (params.keyword) {
    let keyword = "%" + params.keyword.toLowerCase() + "%";

    params.where = {
      [Op.or]: [
        Sequelize.where(Sequelize.fn("lower", Sequelize.col("Authority.name")), { [Op.like]: keyword }),
        Sequelize.where(Sequelize.fn("lower", Sequelize.col("Authority.created_by")), { [Op.like]: keyword }),
        Sequelize.where(Sequelize.fn("lower", Sequelize.col("Authority.updated_by")), { [Op.like]: keyword }),
      ]
    }
  }

  let data = await Authority.findAndCountAll(params);
  let page = pageUtil.getPage(req, data.count);
  return { data, page };
}

async function findOne(req) {
  const authority = await Authority.findOne({
    where: { id: req.params.id }
  });

  if (authority) {
    return authority;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function create(req) {
  try {
    let { name } = req.body;
    let authority = await Authority.create({ name }, { user: req.user });
    await auditLogService.log(AUTHORITY, CREATE, true, authority, req);
    return authority;
  } catch (err) {
    await auditLogService.log(AUTHORITY, CREATE, false, err, req);
    throw err;
  }
}

async function update(req) {
  try {
    let authority = await findOne(req);
    authority.name = req.body.name;

    authority = await authority.save({ user: req.user });
    await auditLogService.log(AUTHORITY, UPDATE, true, authority, req);
    return authority;
  } catch (err) {
    await auditLogService.log(AUTHORITY, UPDATE, false, err, req);
    throw err;
  }
}

async function destroy(req) {
  try {
    let authority = await findOne(req);
    await authority.destroy({ user: req.user });
    await auditLogService.log(AUTHORITY, DELETE, true, authority, req);
  } catch (err) {
    await auditLogService.log(AUTHORITY, DELETE, false, err, req);
    throw err;
  }
}

async function isNameExist(name, id) {
  let authority = await Authority.findOne({
    where: { name }
  });

  if (authority) {
    if (!(id && (id == authority.id))) {
      return Promise.reject(`${name} is already exist`);
    }
  }

  return true;
}

module.exports = {
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  isNameExist
};
