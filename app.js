const appHtml = require('./app-html');
const appRest = require('./app-rest');
const swaggerUi = require('swagger-ui-express');
const path = require('path');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./config/swagger.yaml');

var express = require('express');
var app = express();
var options = {
    customCssUrl: '/stylesheets/swagger-ui-custom.css',
    customSiteTitle: "ExpressJS Starter",
    customfavIcon: "/images/favicon.png",
    customJs: "/javascripts/swagger-ui-custom.js",
    swaggerOptions: {
        url: 'http://localhost:3001/swagger.yaml'
    }
};

app.use((req, res, next) => {
    if (req.url === '/swagger.json') {
        res.sendFile(path.join(__dirname, 'config', '/swagger.json'));
    } else if (req.url === '/swagger.yaml') {
        res.sendFile(path.join(__dirname, 'config', '/swagger.yaml'));
    } else {
        next();
    }
});

app.use(express.static(path.join(__dirname, 'public')));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));
app.use('/api', appRest);
app.use('/', appHtml);

module.exports = app;
