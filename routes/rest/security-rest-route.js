const express = require('express');
const { check, validationResult } = require("express-validator");
const securityService = require('../../services/security-service');
const errorUtil = require("../../util/error-util")


var router = express.Router();
var validate = [
  check("username", "username is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50"),
  check("password", "password is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
]

router.post("/auth", validate, authenticate);
router.get("/revoke", revoke);

async function authenticate(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorUtil.getValidationErrors(errors, res);
    } else {
      let token = await securityService.authenticate(req);

      if (token) {
        return res.status(200).json(token);
      } else {
        return res.status(401).json({ msg: "Invalid username or password" });
      }
    }
  } catch (error) {
    return next(error)
  }

}

async function revoke(req, res, next) {
  try {
    let token = await securityService.revoke(req);
    res.status(200).json(token);
  } catch (error) {
    return next(error)
  }

}

router.get('/protected', (req, res) => {
  res.send('protected');
})

module.exports = router;
