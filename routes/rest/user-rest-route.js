const express = require("express");
const { check, validationResult } = require("express-validator");
const userService = require("../../services/user-service");


var router = express.Router();
var validateUpdate = [
  check("email", "email is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().isEmail()
    .bail().isLength({ max: 50 }).withMessage("chars length max 50"),
  check("username", "username is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
]

var validateCreate = [
  validateUpdate[0], validateUpdate[1],
  check("password", "password is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50"),
  check("passwordConfirmation", "passwordConfirmation is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().custom((value, { req }) => value === req.body.password)
    .withMessage("passwordConfirmation field must have the same value as the password field")
]

router.get("/", findAndCountAll);
router.get("/:id", findOne);
router.post("/", validateCreate, create);
router.put("/:id", update);
router.delete("/:id", destroy);

async function findAndCountAll(req, res, next) {
  try {
    var result = await userService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findOne(req, res, next) {
  try {
    const user = await userService.findOne(req);
    res.status(200).json(user);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const user = await userService.create(req);
      res.status(201).json(user);
    }
  } catch (error) {
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const user = await userService.update(req);
      res.status(200).json(user);
    }
  } catch (error) {
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    await userService.destroy(req);
    res.status(204).send();
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
