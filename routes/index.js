var express = require('express');
var router = express.Router();
var pageUtil = require('../util/page-util');
var db = require('../models/index');
var { Op } = require("sequelize");

/* GET home page. */
router.get('/', search);

async function search(req, res, next){
  try {
    let currentPage = pageUtil.getCurrentPage(req);
    let responseData = {s: '' };

    let params = {
      distinct: true,
      offset: currentPage * pageUtil.PAGE_SIZE,
      limit: pageUtil.PAGE_SIZE
    };

    if (typeof req.query.s !== 'undefined' && req.query.s.trim() !== '') {
      let keyword = '%' + req.query.s + '%';
      responseData.s = req.query.s.trim();

      params.where = {
        [Op.or]: [
          db.sequelize.where(db.sequelize.fn('lower', db.sequelize.col('name')), { [Op.like]: keyword }),
          db.sequelize.where(db.sequelize.fn('lower', db.sequelize.col('createBy')), { [Op.like]: keyword }),
          db.sequelize.where(db.sequelize.fn('lower', db.sequelize.col('lastModifiedBy')), { [Op.like]: keyword }),
        ]
      }
      
    }

    const { count, rows } = await db.authority.findAndCountAll(params);
    const paginateInfo = pageUtil.getPaginateInfo(req, currentPage, count);
    responseData.authorities = rows.map(row => row.toJSON());
    responseData.totalData = count;
    console.log({ ...responseData, ...paginateInfo });
    res.render('index', { ...responseData, ...paginateInfo });
    //res.json({ ...responseData, ...paginateInfo });
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
