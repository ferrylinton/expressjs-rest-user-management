document.addEventListener('readystatechange', event => {

    if (event.target.readyState === "interactive") {
        setTimeout(() => {
            setHighlightCodeWidth();
        }, 300)
    }

});

window.addEventListener('resize', function (event) {
    setTimeout(() => {
        setHighlightCodeWidth();
    }, 50)
});

function setHighlightCodeWidth() {
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var elements = document.getElementsByClassName("highlight-code");

    for (var i = 0; i < elements.length; i++) {
        if (width > 576) {
            elements[i].style.width = (width - 250) + "px";
        }
    }
}
