#!/usr/bin/env node

/**
 * Module dependencies.
 */

require('dotenv').config();

const http = require('http');
const app = require('../app');
const db = require('../models/index');
const dbUtil = require('../util/db-util');
const logger = require("../config/winston");

var port = normalizePort(process.env.PORT || '3000');
var server = null;

db
  .sequelize
  .authenticate()
  .then(createServer, createServerOnError);

dbUtil
  .initDb(db.sequelize);

function createServer(err){
  app.set('port', port);
  server = http.createServer(app);
  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
}

function createServerOnError(err){
  logger.error('Unable to connect to the database:', err);
  logger.error(err.stack);
  process.exit(-1);
}

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = (typeof addr === 'string') ? 'pipe ' + addr : 'port ' + addr.port;
  logger.info('################################################');
  logger.info('#');
  logger.info('#  Listening on ' + bind);
  logger.info('#');
  logger.info('################################################');
}
