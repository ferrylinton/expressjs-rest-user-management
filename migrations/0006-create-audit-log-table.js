"use strict";

const { Sequelize, DataTypes } = require("sequelize");
const tableName = 'audit_log';


module.exports = {

  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, {

      id: {
        type: DataTypes.STRING(11),
        allowNull: false,
        primaryKey: true
      },

      action_type: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      
      action_status: {
        type: DataTypes.STRING(20),
        allowNull: false,
        defaultValue: "STARTED"
      },

      data_id: {
        type: DataTypes.STRING(11),
        allowNull: true
      },

      action_model: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },

      request_body: {
        type: DataTypes.TEXT,
        allowNull: true,
      },

      result: {
        type: DataTypes.TEXT,
        allowNull: true,
      },

      data_hash: {
        type: DataTypes.STRING(100),
        allowNull: false,
        defaultValue: '-'
      },

      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      },

      created_by: {
        type: DataTypes.STRING(70),
        allowNull: false,
        defaultValue: process.env.DEFAULT_USER
      }

    });

  },

  down: async (queryInterface) => {

    await queryInterface.dropTable(tableName);

  }

};