"use strict";

const { Sequelize, DataTypes } = require("sequelize");
const tableName = 'sec_user';


module.exports = {

  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, {

      id: {
        type: DataTypes.STRING(11),
        allowNull: false,
        primaryKey: true
      },

      activated: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0
      },

      locked: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0
      },

      login_attempt_count: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
      },

      username: {
        type: DataTypes.STRING(50),
        allowNull: false
      },

      email: {
        type: DataTypes.STRING(50),
        allowNull: false
      },

      password_hash: {
        type: DataTypes.STRING(250),
        allowNull: false
      },

      role_id: {
        type: DataTypes.STRING(11),
        allowNull: false
      },

      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      },

      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn("now")
      },

      created_by: {
        type: DataTypes.STRING(70),
        allowNull: false,
        defaultValue: process.env.DEFAULT_USER
      },

      updated_by: {
        type: DataTypes.STRING(70),
        allowNull: false,
        defaultValue: process.env.DEFAULT_USER
      }

    });

    await queryInterface.addConstraint(tableName, {
      type: 'foreign key',
      fields: ['role_id'],
      name: 'sec_user_role_id_fk',
      references: {
        table: 'sec_role',
        field: 'id'
      }
    });

    await queryInterface.addConstraint(tableName, {
      type: 'unique',
      fields: ['username'],
      name: 'sec_user_username_uq'
    });

    await queryInterface.addConstraint(tableName, {
      type: 'unique',
      fields: ['email'],
      name: 'sec_user_email_uq'
    });

    await queryInterface.addIndex(tableName, ['created_by'], { name: 'sec_user_created_by' });

    await queryInterface.addIndex(tableName, ['created_at'], { name: 'sec_user_created_at' });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }
  
};