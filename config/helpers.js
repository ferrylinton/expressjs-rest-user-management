const Handlebars = require("handlebars");
const pageUtil = require('../util/page-util');

const list = {

    link: function () {
        arguments = [...arguments].slice(0, -1);
        url = arguments.join('');
        url = Handlebars.escapeExpression(url);
        return new Handlebars.SafeString(url);
    },

    equals: function (string1, string2, options) {
        if (string1 === string2) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    },

    isInvalid: function (field, objects) {
        for (var key in objects) {
            if (objects[key]['param'] === field) {
                return 'is-invalid';
            }
        }

        return '';
    },

    errorsMsg: function (field, objects) {
        for (var key in objects) {
            if (objects[key]['param'] === field) {
                return objects[key]['msg'];
            }
        }

        return '';
    },

    concat: function () {
        var result = '';

        for (var i in arguments) {
            result += (typeof arguments[i] === 'string' ? arguments[i] : '') + '';
        }

        return result.trim();
    },

    add: function (val1, val2) {
        if (Number.isInteger(val1) && Number.isInteger(val2)) {
            return parseInt(val1) + parseInt(val2);
        } else {
            return 'error:' + val1 + ',' + val2;
        }
    },

    rownum: function (index, currentPage) {
        if (Number.isInteger(index) && Number.isInteger(currentPage)) {
            return parseInt(index) + 1 + (parseInt(currentPage) * pageUtil.PAGE_SIZE);
        } else {
            return 'error:' + index + ',' + currentPage;
        }
    },

    addClass: function (condition, val1, val2) {
        if (condition) {
            return val1;
        } else {
            return val2;
        }
    },

    setRootVar: function (varName, varValue, options) {
        if (varValue) {
            options.data.root[varName] = varValue.split(',');
        } else {
            options.data.root[varName] = [];
        }
    },

    eq: function (arg1, arg2, options) {
        return arg1 === arg2;
    },

    ne: function (arg1, arg2, options) {
        return arg1 !== arg2;
    },

    isSelected: function (path, options) {
        console.log(options.data.root.expreq);
        console.log(path);
        if (options.data.root.expreq && options.data.root.expreq.path === path) {
            return 'selected';
        } else {
            return '';
        }
    },

    json: function(context) {
        return JSON.stringify(context);
    },

    trim: function(str) {
        return typeof str === 'string' ? str.trim() : '';
    }

}

module.exports = {
    list: list
};