const logger = require('../config/winston');

module.exports = {
  "development": {
    "username": process.env.DEVELOPMENT_USERNAME,
    "password": process.env.DEVELOPMENT_PASSWORD,
    "database": process.env.DEVELOPMENT_DATABASE,
    "host": process.env.DEVELOPMENT_HOST,
    "dialect": process.env.DEVELOPMENT_DIALECT,
    "logging": (msg) => logger.debug(msg),
    "storage": process.env.DEVELOPMENT_STORAGE,
    "define": {
      timestamps: false,
      underscored: true
    }
  },
  "test": {
    "username": "root",
    "password": "password",
    "database": "test",
    "host": "localhost",
    "dialect": "sqlite",
    "logging": false,
    "define": {
      timestamps: false,
      underscored: true
    }
  },
  "production": {
    "username": process.env.PRODUCTION_USERNAME,
    "password": process.env.PRODUCTION_PASSWORD,
    "database": process.env.PRODUCTION_DATABASE,
    "host": process.env.PRODUCTION_HOST,
    "dialect": process.env.PRODUCTION_DIALECT,
    "logging": process.env.PRODUCTION_LOGGING === 'true',
    "define": {
      timestamps: false,
      underscored: true
    }
  }
} 