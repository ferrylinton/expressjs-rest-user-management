
const ActionType = {
    CREATE: "CREATE",
    UPDATE: "UPDATE",
    DELETE: "DELETE"
}

const ActionStatus = {
    SUCCESS: "SUCCESS",
    ERROR: "ERROR"
}

const ActionModel = {
    AUTHORITY: "Authority",
    ROLE: "Role",
    USER: "User"
}

module.exports = {
    ActionType,
    ActionStatus,
    ActionModel
};