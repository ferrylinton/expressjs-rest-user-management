"use strict";

const { Model } = require("sequelize");
const { uid } = require("uid");

module.exports = (sequelize, DataTypes) => {

  class Authority extends Model {

    static associate(models) {
      // define association here
    }

  };

  Authority.init({

    id: {
      type: DataTypes.STRING(11),
      primaryKey: true
    },

    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        is: {
          args: /^[A-Za-z0-9_-]+$/,
          msg: "must contains alphanumeric, -, or _"
        },
        len: {
          args: [3, 50],
          msg: "chars length min 3 and max 50"
        }
      }
    },

    createdAt: {
      type: DataTypes.DATE
    },

    updatedAt: {
      type: DataTypes.DATE
    },

    createdBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    },

    updatedBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    }

  }, {
    sequelize,
    modelName: "Authority",
    tableName: "sec_authority",
    hooks: {

      beforeCreate: (instance, options) => {
        let user = options.user.id + "," + options.user.username;
        let now = new Date();

        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;
        instance.createdBy = user;
        instance.updatedBy = user;
      },

      beforeUpdate: (instance, options) => {
        instance.updatedAt = new Date();
        instance.updatedBy = options.user.id + "," + options.user.username;
      }
      
    }
  });

  return Authority;

};