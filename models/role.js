"use strict";

const { Model } = require("sequelize");
const { uid } = require("uid");

module.exports = (sequelize, DataTypes) => {

  class Role extends Model {

    static associate(models) {

      models.Role.hasMany(models.User, {
        foreignKey: "roleId"
      });

      models.Role.belongsToMany(models.Authority, {
        through: "RoleAuthority",
        foreignKey: "roleId",
        as: "authorities"
      });

    }

  };

  Role.init({

    id: {
      type: DataTypes.STRING(11),
      primaryKey: true
    },

    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        is: {
          args: /^[A-Za-z0-9]+$/,
          msg: "must contains alphanumeric"
        },
        len: {
          args: [3, 50],
          msg: "chars length min 3 and max 50"
        }
      }
    },

    createdAt: {
      type: DataTypes.DATE
    },

    updatedAt: {
      type: DataTypes.DATE
    },

    createdBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    },

    updatedBy: {
      type: DataTypes.STRING(70),
      defaultValue: process.env.DEFAULT_USER
    }

  }, {
    sequelize,
    modelName: "Role",
    tableName: "sec_role",
    hooks: {

      beforeCreate: (instance, options) => {
        let user = options.user.id + "," + options.user.username;
        let now = new Date();

        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;
        instance.createdBy = user;
        instance.updatedBy = user;
      },

      beforeUpdate: (instance, options) => {
        instance.updatedAt = new Date();
        instance.updatedBy = options.user.id + "," + options.user.username;
      }
      
    }
  });

  return Role;

};