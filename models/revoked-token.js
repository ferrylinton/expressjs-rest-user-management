"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {

  class RevokedToken extends Model {

    static associate(models) {
    }

  };

  RevokedToken.init({

    id: {
      type: DataTypes.STRING(11),
      allowNull: false,
      primaryKey: true
    },

    token: {
      type: DataTypes.TEXT,
      allowNull: false
    },

    username: {
      type: DataTypes.STRING(50),
      allowNull: false
    },

    createdAt: {
      type: DataTypes.DATE
    }

  }, {
    sequelize,
    modelName: 'RevokedToken',
    tableName: 'sec_revoked_token'
  });

  return RevokedToken;
};