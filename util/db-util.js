const Umzug = require('umzug');
const path = require('path');
const logger = require("../config/winston");

function initDb(sequelize) {
    const migrations = new Umzug({
        migrations: {
            path: path.join(process.cwd(), './migrations'),
            params: [
                sequelize.getQueryInterface()
            ]
        },
        storage: 'sequelize',
        storageOptions: {
            sequelize: sequelize
        }
    });

    const seeders = new Umzug({
        migrations: {
            path: path.join(process.cwd(), './seeders'),
            params: [
                sequelize.getQueryInterface()
            ]
        },
        storage: 'sequelize',
        storageOptions: {
            sequelize: sequelize
        }
    });

    up(migrations, seeders);
}

async function up(migrations, seeders) {
    try {
        await migrations.up();
        logger.info('All migrations performed successfully');

        await seeders.up()
        logger.info('All seeders performed successfully')
    } catch (err) {
        logger.error(err.stack);
        process.exit(-1);
    }

}


module.exports = {
    initDb: initDb
};