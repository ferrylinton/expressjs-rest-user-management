const lodash = require("lodash");

const PAGE_SIZE = 10;

function getParams(req) {
    let params = { limit: PAGE_SIZE };
    let { page, keyword } = req.query;

    params.current = lodash.toInteger(page);
    params.current = params.current > 0 ? params.current : 1;
    params.offset = (params.current - 1) * PAGE_SIZE;

    if (!lodash.isUndefined(keyword) && keyword.trim() !== "") {
        params.keyword = keyword.trim();
    }

    req.params = params;
    return params;
}

function getPage(req, count) {
    let size = PAGE_SIZE;
    let keyword = req.params.keyword;
    let searchParam = (keyword) ? `keyword=${keyword}&` : "";

    let current = req.params.current;
    let total = Math.ceil(count / PAGE_SIZE);

    let hasPrevious = current > 1;
    let previousUrl = hasPrevious ? `${req.path}?${searchParam}page=${current - 1}` : `${req.path}?${searchParam}`;

    let hasNext = current < (total);
    let nextUrl = hasNext ? `${req.path}?${searchParam}page=${current + 1}` : `${req.path}?${searchParam}`;

    return {
        size,
        total,
        current,
        hasPrevious,
        hasNext,
        previousUrl,
        nextUrl,
        keyword
    }
}

module.exports = {
    getParams: getParams,
    getPage: getPage
};