var createError = require('http-errors');
var express = require('express');
var exphbs = require('express-handlebars');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var logger = require('./config/winston');

var helpers = require('./config/helpers');

var indexRouter = require('./routes/index');
var authorityRouter = require('./routes/authority-route');

var app = express();
var hbs = exphbs.create({
  layoutsDir: path.join(__dirname, 'views', 'layouts'),
  partialsDir: path.join(__dirname, 'views', 'partials'),
  defaultLayout: 'main',
  extname: '.hbs',
  helpers: helpers.list
});

app.engine('.hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('helpers', helpers.list);

app.use(morgan('combined', { stream: logger.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'node_modules', 'bootstrap')));
app.use(express.static(path.join(__dirname, 'node_modules', 'jquery')));
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/authorities', authorityRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  console.log("####################### HTML error");
  res.status(err.status || 500);
  res.locals.message = err.message;
  res.render('error');
});

module.exports = app;
