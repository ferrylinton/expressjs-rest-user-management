# ExpressJS - Handlebars - SQlite3

## Requirement

    - Nodejs
    - Nodemon

## Run

## Run Development
```
npn run dev
```

## Troubleshoot

### Internal watch failed: ENOSPC: System limit for number of file watchers reached
```
echo fs.inotify.max_user_watches=582222 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```